import { createContext, useContext, useState } from 'react';

const ThemeContext = createContext();
const ThemeChangeContext = createContext();

export const useTheme = () => {
  return useContext(ThemeContext);
}

export const useThemeChange = () => {
  return useContext(ThemeChangeContext);
}

export const ThemeProvider = ({ children }) => {
  const [ darkTheme, setDarkTheme ] = useState(false);

  function toggleTheme() {
    setDarkTheme(prevDarkTheme => !prevDarkTheme);
  }

  return (
    <ThemeContext.Provider value={darkTheme}>
      <ThemeChangeContext.Provider value={toggleTheme}>
        { children }
      </ThemeChangeContext.Provider>
    </ThemeContext.Provider>
  )
}
