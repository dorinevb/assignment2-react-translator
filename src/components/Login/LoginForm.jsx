import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { useUser } from '../../context/UserContext';
import { loginUser } from '../../api/user';
import { saveStorage } from '../../utils/storage';
import { STORAGE_KEY_USER } from '../../const/storageKeys';

// Styles
import './LoginForm.css';

const usernameConfig = {
  required: true,
  minLength: 6
}

const LoginForm = () => {
  // Hooks
  const { register, handleSubmit, formState: { errors } } = useForm();
  const { user, setUser } = useUser();
  const navigate = useNavigate();
  
  // Local state
  const [ loading, setLoading ] = useState(false);
  const [ apiError, setApiError ] = useState(null);

  // Side effects
  useEffect(() => {
    if (user !== null) {
      navigate('translation');
    }
  }, [ user, navigate ]);

  // Event handlers
  const onSubmit = async ({ username }) => {
    setLoading(true);
    if (username.trim().length > 5) {
      const [ error, userResponse ] = await loginUser(username);
      if (error !== null) {
        setApiError(error);
      }
      if (userResponse !== null) {
        saveStorage(STORAGE_KEY_USER, userResponse);
        setUser(userResponse);
      }
    } else {
      alert("Please enter a valid user name of min. 6 characters");
    }
    setLoading(false);
  };

  // Render functions
  const loginErrorMessage = (() => {
    if (!errors.username) {
      return null;
    }
    if (errors.username.type === "required") {
      return <span className="LoginError">Please enter a username</span>;
    }
    if (errors.username.type === "minLength") {
      return <span className="LoginError">Username should be 6 characters or more</span>;
    }
  })();

  return (
    <>
      <h2 className="LoginRequest">Please enter your name:</h2>
      <form onSubmit={ handleSubmit(onSubmit) } className="Form LoginForm">
        <fieldset>
          <label htmlFor="username" className="FormLabel">Username: </label>
          <input 
            type="text"
            placeholder="geraltofrivia"
            { ...register("username", usernameConfig) } 
            className="FormInput" />
          { loginErrorMessage }
        </fieldset>
        <button type="submit" disabled={loading} className="Button LoginButton">log in</button>
      </form>
      { loading && <p className="Loading">Loading...</p> }
      { apiError && <p>{ apiError }</p> }
    </>
  );
};

export default LoginForm;