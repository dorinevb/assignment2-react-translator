import { clearTranslationHistory } from "../../api/translation";
import { useUser } from "../../context/UserContext";
import { saveStorage } from "../../utils/storage";
import { STORAGE_KEY_USER } from "../../const/storageKeys";

// Styles
import './ProfileHistory.css';

// Components
import ProfileHistoryItem from "./ProfileHistoryItem";

const ProfileHistory = ({ translations }) => {

  let lastTranslations = [];
  // Only display 10 most recent translations
  if(translations.length > 10) {
    lastTranslations = [...translations].slice(translations.length - 10);
  } else {
    lastTranslations = translations;
  }
  const translationList = lastTranslations.map((translation, index) => <ProfileHistoryItem key={ index + "-" + translation } translation={ translation }/>);

  const { user, setUser } = useUser();
  
  const handleClearHistory = async () => {
    if (!window.confirm("Are you sure you want to delete the translation history?\nThis can not be undone.")) {
      return;
    }
    const [ clearError, clearResult ] = await clearTranslationHistory(user.id);
    if (clearError !== null) {
      return;
    }
    saveStorage(STORAGE_KEY_USER, clearResult);
    setUser(clearResult);
  }

  return (
    <section className="ProfileHistory">
      <h2>My most recent translations</h2>
      { translationList.length === 0 && <p>You have no translations yet.</p>}
      <ul>{ translationList }</ul>
      <button onClick={ handleClearHistory } className="Button ClearHistoryButton">clear history</button>
    </section>
  );
}

export default ProfileHistory;