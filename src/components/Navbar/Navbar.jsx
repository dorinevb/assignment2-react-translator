import { NavLink } from "react-router-dom";
import { useUser } from '../../context/UserContext';
import { deleteStorage } from "../../utils/storage";

// Styles
import './Navbar.css';

//Constants
import { STORAGE_KEY_USER } from "../../const/storageKeys";

const Navbar = () => {

  const { user, setUser } = useUser();

  const handleLogout = () => {
    if(window.confirm("Are you sure you want to log out?")) {
      deleteStorage(STORAGE_KEY_USER);
      setUser(null);
    }
  };

  return (
    <nav className={user !== null ? "Navbar NavbarLoggedIn" : "Navbar NavbarLoggedOut"}>
      {/* Tiger icons created by Freepik - Flaticon https://www.flaticon.com/free-icons/tiger" */}
      <img src="img/tiger.png" alt="cute tiger head" className="Logo NavbarLogo"></img>
      <h1 className="NavbarTitle">Tommy The Translation Tiger</h1>
      { user !== null &&
        <ul>
          <li className="NavbarLink"><NavLink to="/profile">Profile</NavLink></li>
          <li className="NavbarLink"><NavLink to="/translation">Translation</NavLink></li>
          <li className="NavbarLink"><button onClick={ handleLogout } className="Button LogoutButton">Log out</button></li>
        </ul> 
      }
    </nav>
  )
};

export default Navbar;