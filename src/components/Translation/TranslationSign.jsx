import './TranslationSign.css';

const TranslationSign = ({ src }) => {
  return (
    <img className="TranslationSign" src={src} alt="sign" />
  );
};

export default TranslationSign;