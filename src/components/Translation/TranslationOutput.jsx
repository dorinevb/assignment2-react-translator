import './TranslationOutput.css';

// Components
import TranslationSign from "./TranslationSign";

const TranslationOutput = ({ textToTranslate }) => {

  // Change text to lower case and replace any characters that are not letters by '1'
  // All letters have a corresponding image, the image for '1' is a blank square.
  const cleanText = textToTranslate.toLowerCase().replace(/[^a-z]/gm, "1");
  const letterList = cleanText.split("");
  const signSequence = letterList.map((letter, index) => <TranslationSign key={index} src={`img/${letter}.png`} />);
    
  return (
    <>
      <div className="TranslationOutputBox">
        <p>{ signSequence }</p>
      </div>
    </>
  );
};

export default TranslationOutput;