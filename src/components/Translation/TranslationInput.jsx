import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { addTranslation } from '../../api/translation';
import { STORAGE_KEY_USER } from '../../const/storageKeys';
import { useUser } from '../../context/UserContext';
import { saveStorage } from '../../utils/storage';

// Styles
import './TranslationInput.css';

const translationConfig = {
  required: true
};

const TranslationInput = ({ sendTextToOutput }) => {

  //Hooks
  const { register, handleSubmit, formState: { errors } } = useForm();
  const { user, setUser } = useUser();

  // Local state
  const [ loading, setLoading ] = useState(false);
  
  // Event handlers
  const onSubmit = async ({ translationInput }) => {
    setLoading(true);
    if (translationInput.trim().length === 0) {
      alert("Please enter valid text");
      setLoading(false);
      return;
    }
    const [ error, updatedUser ] = await (addTranslation(user, translationInput));
    if (error !== null) {
      setLoading(false);
      return;
    }
    // Keep UI state and server state in sync
    saveStorage(STORAGE_KEY_USER, updatedUser);
    // Update context state
    setUser(updatedUser);
    // Trigger callback function to send text to output box
    sendTextToOutput(translationInput);
    setLoading(false);
  };

  // Returns error message when user clicks on translate button without entering text first
  const inputErrorMessage = (() => {
    if (!errors.translationInput) {
      return null;
    }
    if (errors.translationInput.type === "required") {
      return <span className="InputError">Please enter some text</span>;
    }
  })();

  return (
    <>
      <form onSubmit={ handleSubmit(onSubmit) } className="Form TranslationForm" >
        <fieldset>
          <label htmlFor="translation-input" className="FormLabel">Enter input: </label>
          <input 
            type="text" 
            { ...register('translationInput', translationConfig) }
            placeholder="Input text in English"
            className="FormInput TranslationFormInput" />
          { inputErrorMessage }
        </fieldset>
        <button type="submit" className="Button TranslationButton">translate</button>
      </form>
      { loading ? <p className="Loading">Loading...</p> : <p className="Loading"></p>}
    </>
  );
}

export default TranslationInput;