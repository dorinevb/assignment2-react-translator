import LoginForm from "../components/Login/LoginForm";

// Styles
import './Login.css';

const Login = () => {

  return (
    <div className="LoginView">
      {/* Deaf icons created by Flat Icons */}
      <h1>Login</h1>
      <LoginForm />
    </div>
  );
};

export default Login;
