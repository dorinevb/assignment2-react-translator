
import withAuth from '../hoc/withAuth';
import { useUser } from '../context/UserContext';

// Styles
import './Profile.css';

// Components
import ProfileHeader from '../components/Profile/ProfileHeader';
import ProfileHistory from '../components/Profile/ProfileHistory';

const Profile = () => {

  const { user } = useUser();

  return (
    <div className="ProfileView">
      <ProfileHeader username={ user.username }/>
      <ProfileHistory translations={ user.translations }/>
    </div>
  );
};

export default withAuth(Profile);