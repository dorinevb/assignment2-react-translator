import { useState } from 'react';
import withAuth from "../hoc/withAuth";

// Styles
import './Translation.css';

// Components
import TranslationInput from "../components/Translation/TranslationInput";
import TranslationOutput from "../components/Translation/TranslationOutput";

const Translation = () => {

  // local state for text to be translated, text is send to child (TranslationOutput)
  const [ textToTranslate, setTextToTranslate ] = useState("");

  // Callback function that is triggered in the TranslationInput child
  // upon succesful input in the form
  const sendTextToOutput = (textForOutput) => {
    setTextToTranslate(textForOutput);
  }

  return (
    <div className="TranslationView">
      <section id="TranslationInput">
        <TranslationInput sendTextToOutput={ sendTextToOutput }/>
      </section>
      <section id="TranslationOutput">
        <TranslationOutput textToTranslate={ textToTranslate }/>
      </section>
    </div>
  );
}

export default withAuth(Translation);