const checkKey = (key, functionName) => {
  if (!key) {
    throw new Error(`${functionName}: No storage key provided`);
  }
  if (typeof key !== "string") {
    throw new Error(`${functionName}: Invalid storage key provided`);
  }
}

export const saveStorage = (key, value) => {
  checkKey(key, "saveStorage");
  if (!value) {
    throw new Error("saveStorage: No storage value provided");
  }
  sessionStorage.setItem(key, JSON.stringify(value));
};

export const readStorage = key => {
  checkKey(key, "readStorage");
  const data = sessionStorage.getItem(key);
  if (data) {
    return JSON.parse(data);
  }
  return null;
};

export const deleteStorage = key => {
  checkKey(key, "deleteStorage");
  sessionStorage.removeItem(key);
};