import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useTheme, useThemeChange } from './context/ThemeContext';

// Styles
import './App.css';

// Components
import Navbar from './components/Navbar/Navbar';
import Login from './views/Login';
import Translation from './views/Translation';
import Profile from './views/Profile';

function App() {

  const darkTheme = useTheme();
  const toggleTheme = useThemeChange();
  const themeStyles = {
    backgroundColor: darkTheme ? '#171717' : 'white',
    color: darkTheme ? 'white' : '#171717'
  }

  return (
      <BrowserRouter>
        <div className="App" style={themeStyles}>
          <Navbar />
          <button onClick={toggleTheme} className="Button ThemeButton">dark theme </button>
          <Routes>
            <Route path="/" element={ <Login /> } />
            <Route path="/translation" element={ <Translation /> } />
            <Route path="/profile" element={ <Profile /> } />
          </Routes>
        </div>
      </BrowserRouter>
  );
}

export default App;
