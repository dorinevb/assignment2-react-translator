import { createHeaders } from './index';

const url = process.env.REACT_APP_API_URL;

// Check if user already exists
const checkForUser = async (username) => {
  try {
    const response = await fetch(`${url}?username=${username}`);
    if (!response.ok) {
      throw new Error("Could not complete request");
    }
    const data = await response.json();
    return [ null, data ];
  }
  catch (error) {
    return [ error.message, [] ];
  }
}

// Create new user
const createUser = async (username) => {
  try {
    const response = await fetch(url, {
      method: "POST",
      headers: createHeaders(),
      body: JSON.stringify({
        username,
        translations: []
      })
    });
    if (!response.ok) {
      throw new Error(`Could not create user with username ${username}`);
    }
    const data = await response.json();
    return [ null, data ];
  }
  catch (error) {
    return [ error.message, [] ];
  }
}

// Returns either existing user of newly created user (or error)
export const loginUser = async (username) => {
  const [ checkError, user ] = await checkForUser(username);
  if (checkError !== null) {
    return [ checkError, null ];
  }
  if (user.length > 0) {
    return [ null, user.pop() ];
  }
  return await createUser(username);
}

// Returns user by id
export const findUserById = async (userId) => {
  try {
    const response = await fetch(`${url}/${userId}`);
    if (!response.ok) {
      throw new Error("Could not find user by id");
    }
    const user = await response.json();
    return [ null, user ];
  }
  catch (error) {
    return [ error.message, null ];
  }
}