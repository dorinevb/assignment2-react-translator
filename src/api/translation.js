import { createHeaders } from './index';

const url = process.env.REACT_APP_API_URL;

// Adds new translation to API
export const addTranslation = async (user, translation) => {
  try {
    const response = await fetch(`${url}/${user.id}`, {
      method: "PATCH",
      headers: createHeaders(),
      body: JSON.stringify({
        translations: [...user.translations, translation]
      })
    });

    if (!response.ok) {
      throw new Error("Could not add the translation");
    }
    const result = await response.json();
    return [ null, result ];
  }
  catch (error) {
    return [ error.message, null ];
  }

};

// Clears list of translations for current user
export const clearTranslationHistory = async (userId) => {
  try {
    const response = await fetch(`${url}/${userId}`, {
      method: "PATCH",
      headers: createHeaders(),
      body: JSON.stringify({
        translations: []
      })
    });
    if (!response.ok) {
      throw new Error("Could not clear translations");
    }
    const result = await response.json();
    return [ null, result ];
  } catch (error) {
    return [ error.message, null ];
  }
};