# React Sign Language Translator App

This React App translates written English language to American sign language. It is connected to an API on Heroku, to fetch and temporarily store user data.

This repository contains:    

1. The source code written in JavaScript, HTML and CSS, using the React framework.
2. A PDF of a component tree, which shows the structure of the app.

## Background

I created this app as an assignment for the frontend course of Noroff School of Technology and Digital Media, which is part of the software development traineeship at Experis (Manpower Group). The goal of the assignment is to improve my skills in React.

## Install

This project uses node and npm, please make sure you have them installed.
After downloading the source code, run `npm install` to install the required node packages for this app.
Then run `npm start`.

## Usage

This app consists of three views:
1. The Login View: here you can enter your username (min. 6 characters) and log in. The app checks if the user is already in the api and otherwise creates a new user.
2. The Translation View: here you can enter English words and sentences and click the translation button. The American sign language will then appear in the translation output box. The words you enter are temporarily (30 min) saved in the Heroku API. Spaces and special characters are displayed as whitespace.
3. The Profile View: here you can see the last 10 words / sentences that you requested to be translated. By clicking the clear button you can remove all translations from this page and from the API. 

The app also has a Navbar component, which can be used to navigate between pages and to log out, via the logout button. 

Directly below the navbar is a button that allows you to toggle between a dark theme and light theme for the webpage. 

## Contributors

Dewald Els

## License

MIT

Copyright 2022 Dorine van Belzen

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
